# zines

This repository contains the zines we print to distro here and there. We try to keep each zine in three version: the full zine, only the cover and only the gots, so if we want to print the cover on better paper it's easier to do.

If you have pdfjam installed on your machine you can run the following to extract the cover:

```bash
pdfjam --landscape --quiet zine.pdf 1-2 --outfile zine-cover.pdf
```

If you want to extract the guts, you can run the following command:

```bash
pdfjam --landscape --quiet zine.pdf 3- --outfile zine-guts.pdf
```

## sources

* https://www.sproutdistro.com/
* https://fillerpgh.wordpress.com/archive/
* https://crimethinc.com/zines
* https://haters.noblogs.org/zines/
* https://ftpdistro.noblogs.org/
* https://creekerzine.wordpress.com/
* https://theanarchistlibrary.org/
